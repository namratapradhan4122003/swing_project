package quiz.application;

import java.awt.Color;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;


public class Login extends JFrame implements ActionListener {
    JButton rules,back;
    JTextField tfname;

    Login() {
        getContentPane().setBackground(Color.WHITE);//It is used to set background of frame
        setLayout(null);
        ImageIcon i1=new ImageIcon(ClassLoader.getSystemResource("icons/login.jpeg"));
        JLabel image=new JLabel(i1);
        image.setBounds(0,0,700,600);
        add(image);
        
        JLabel heading=new JLabel("Simple Minds");
        heading.setBounds(800,150,300,45);
        heading.setFont(new Font("Viner Hand ITC",Font.BOLD,40));
        heading.setForeground(new Color(30,144,254));
        add(heading);
        
        
        JLabel name=new JLabel("Enter Your Name");
        name.setBounds(860,240,300,25);
        name.setFont(new Font("Mongolian Baiti",Font.BOLD,20));
        name.setForeground(new Color(30,144,254));
        add(name);
        
        tfname=new JTextField();
        tfname.setBounds(800,290,300,30);
        tfname.setFont(new Font("Times New Roman",Font.BOLD,16));
        add(tfname);
        
         rules=new JButton("Rules");
        rules.setBounds(800,370,120,30);
        rules.setBackground(new Color(30,144,254));
        rules.setForeground(Color.WHITE);
        rules.addActionListener(this);
        add(rules);
        
        back=new JButton("Back");
        back.setBounds(970,370,120,30);
        back.setBackground(new Color(30,144,254));
        back.setForeground(Color.WHITE);
        back.addActionListener(this);
        add(back);
        
        setSize(1500,800); //it takes two argument length and width
        setLocation(0,0);  //left 200 and top 200
        setVisible(true);    //frame is hidden in file to see that we use setvisible function
}
    
    public void actionPerformed(ActionEvent ae){
        if(ae.getSource()==rules){
            String name=tfname.getText();
            setVisible(false);
            new Rules(name);
            
        }
        else if(ae.getSource()==back){
            setVisible(false);
    }
   }
    

public static void main(String[] args){
    new Login();
}
}




